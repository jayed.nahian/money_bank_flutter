import 'package:flutter/material.dart';

class BorderShadow extends StatelessWidget {
  final card_bg_color = const Color(0xFFA7AAB2);

  @override
  Widget build(BuildContext context) {
    return Container(
     decoration:BoxDecoration(
         color: card_bg_color,
         borderRadius: BorderRadius.circular(15),
         boxShadow: [
           BoxShadow(
             color: Color.fromARGB(255, 65, 64, 64),
             spreadRadius: 2,
             blurRadius: 2,
             offset: Offset(
                 0, 3), // changes position of shadow
           )
         ]
     )
    );
  }
}
