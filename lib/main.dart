import 'package:flutter/material.dart';
import 'package:moneybank_2/model_class/transection.dart';
// import 'widgets/user_transection.dart';

//custom widgets
import '/widgets/transection_list_widget.dart';
import '/widgets/new_transection_input_fiels.dart';
import 'widgets/cart.dart';
import 'widgets/transeciotn_list_new.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final bg_color = const Color(0xFF7A818A);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My app',
      home: MyHomePage(),
      theme: ThemeData(
          primarySwatch: Colors.blueGrey,
          fontFamily: 'Quicksand',
          appBarTheme: AppBarTheme(
              textTheme: ThemeData.light().textTheme.copyWith(
                  headline6: TextStyle(
                      fontFamily: 'OpenSans',
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                  )
              )
          )
      ),
    );
  }
}
class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double flull_width = double.infinity;
  double height = 100;
  final titleController = TextEditingController();
  final amountController = TextEditingController();

  // all transfr from user transection
  final List<Transaction> _userTransection = [
    // Transaction(
    //     id: 't1', title: "Buy new", amount: 995.45, date: DateTime.now()),
    // Transaction(
    //     id: 't2', title: "Buy T-shirt", amount: 85.45, date: DateTime.now()),
  ];

  //cart function
  List<Transaction> get _recentTransactions{
    return _userTransection.where((tx){
      return tx.date!.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
      //102 updated
    }).toList();
  }

  // ad new transection object
  void _addNewTransction(
      String txTitle, double txAmount) // this two paremeter wii update
  {
    final newTx = Transaction(
        title: txTitle,
        amount: txAmount,
        date: DateTime.now(),
        id: DateTime.now().toString());
    setState(() {
      _userTransection.add(newTx);
    });
  }

  void _startAddNewTransection(BuildContext ctx) {
    //context
    //take context as argument to show modal sheet !
    //builder
    // retrun a widgets which return modal bottom sheet!

    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return GestureDetector(
            child: NewTransection(_addNewTransction),
            // reduce outside tap functions!
            onTap: () => {},
            behavior: HitTestBehavior.opaque,

          );
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Expance"),
        actions: [
          IconButton(
            icon: Icon(
              Icons.add,
              color: Colors.white70,
              size: 40.0,
            ),
            onPressed: () =>_startAddNewTransection(context),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // Container(
            //   width: flull_width,
            //   // height:height,
            //   child: Card(
            //     color: Colors.blue,
            //     child: Text('CART!'),
            //     elevation: 5,
            //   ),
            // ),
            // custom ! widgets!
            // Cart(recentTransaction),
            Cart(_recentTransactions),
            // TransctionList(_userTransection),
            NewTransectionList(_userTransection)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          // color: Colors.yellow,
          size: 40.0,
        ),
        onPressed: () =>_startAddNewTransection(context),
      ),
    );
  }
}
