

import 'package:flutter/material.dart';


class CartBar extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPctOfTotal;

  CartBar(this.label, this.spendingAmount, this.spendingPctOfTotal);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // define the total amount !
        FittedBox(
          child: Text('\$${spendingAmount.toStringAsFixed(0)}'),
        ),
        SizedBox(
          height: 4,
        ),
        Container(
          height: 70,
          width: 15,
          decoration: BoxDecoration(

            boxShadow: [
              BoxShadow(
                color: Color.fromARGB(255, 65, 64, 64),
                spreadRadius:1,
                blurRadius: 5,
                offset: Offset(0, 1),
              )
            ]
          ),
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(

                  border: Border.all(color: Colors.grey, width: 1.0),
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              FractionallySizedBox(
                heightFactor: spendingPctOfTotal,
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(3),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Text(label),
      ],
    );
  }
}

