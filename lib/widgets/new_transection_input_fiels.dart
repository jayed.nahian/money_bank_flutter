
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class NewTransection extends StatefulWidget {
  //button Onpress
  final Function addTxFunction;

  NewTransection(this.addTxFunction);

  @override
  State<NewTransection> createState() => _NewTransectionState();
}

class _NewTransectionState extends State<NewTransection> {

  final card_bg_color = const Color(0xFFA7AAB2);
  final card_title_color = const Color(0xFF7A818A);
  final full_bg_color = const Color(0xFFA7AAB2);

  final titleController = TextEditingController();
  final amountController = TextEditingController();

  // will get the value once the user select the date!
  DateFormat? _selectedDateTime;


  // the function will run on submit
  void submitData() {
    final enterTitle = titleController.text;
    final enterAmount = double.parse(amountController.text);
    if (enterTitle.isEmpty || enterAmount <= 0) {
      return null;
    }
    //widget get access to class its self
    widget.addTxFunction(enterTitle, enterAmount);

    //94 adding this close the top most context after submission
    //context givess access to any context of current widgets!
    Navigator.of(context).pop();
  }
  //111
  void _presentDatePickerFunction(){
    showDatePicker(
        context:context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2019),
        lastDate: DateTime.now()).then((pickedDate){
          if (pickedDate == null){
            return;
          }
          // if the statement is pasued!
          _selectedDateTime = pickedDate as DateFormat;

    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4,
      child: Container(
        color: Colors.white24,
        padding: EdgeInsets.all(5),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
                width: 5, color:card_title_color
            ),
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(80),
              bottomLeft: Radius.circular(80)
            ),
          ),
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                //title input
                TextField(
                  style: TextStyle(fontSize: 20),
                  controller: titleController,
                  decoration: InputDecoration(
                    labelText: 'Title',
                  ),
                  onSubmitted: (field_value) => submitData,
                ),
                //amount input
                TextField(
                  keyboardType: TextInputType.number,
                  style: TextStyle(fontSize: 20),
                  controller: amountController,
                  decoration: InputDecoration(labelText: 'Amount'),
                  onSubmitted: (field_value) => submitData,
                ),
                //date picker
                Container(
                  height: 100,
                  child: Row(
                    children: [
                      //111
                      Text(
                          _selectedDateTime == null
                              ?'No date choosen'
                              : 'Picked Date: ${DateFormat.yMd().format(_selectedDateTime as DateTime)}'
                      ),
                      FlatButton(
                          textColor: Theme.of(context).primaryColor,
                          onPressed:_presentDatePickerFunction,
                          child: Text(
                            'Choose Date',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold
                            ),)
                      )
                    ],
                  ),
                ),

                //submit button
                ElevatedButton(
                    onPressed: submitData,

                    child: Text(
                      "add money",
                      style: TextStyle(fontSize: 20),

                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
