import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';


//custom wigets
import '../model_class/transection.dart';
import '/widgets/empty_data.dart';

//desigen widget
import '/Desigen_widgets/broder_shadow.dart';


class NewTransectionList extends StatelessWidget {
  
  //colors
  final card_bg_color = const Color(0xFFA7AAB2);
  final card_title_color = const Color(0xFF7A818A);
  final full_bg_color = const Color(0xFFA7AAB2);
  
  final List<Transaction> transactions;


  NewTransectionList(this.transactions);




  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: transactions.isEmpty?
      EmptyData():
          ListView.builder(itemBuilder: (content,index){
            return Container(

              margin:EdgeInsets.all(10),
              padding:EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      bottomLeft: Radius.circular(20)
                  ),
                  // borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromARGB(255, 65, 64, 64),
                      spreadRadius: 2,
                      blurRadius: 2,
                      offset: Offset(
                          0, 3), // changes position of shadow
                  )
                ]
              ),
              child: ListTile(
                horizontalTitleGap: 5,

                leading:Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: 1,
                          // color: Theme.of(context).primaryColorLight,
                          color: Colors.white60
                      ),
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromARGB(255, 65, 64, 64),
                        spreadRadius: 2,
                        blurRadius: 2,
                        offset: Offset(
                            2, 3), // changes position of shadow
                      )
                    ]
                  ),
                  child: CircleAvatar(
                    radius: 35,
                    backgroundColor: card_bg_color,
                    child: Padding(
                      padding:EdgeInsets.all(10),
                      child: Container(
                        child:FittedBox(
                            child: Text('\$ ${transactions[index].amount!.toStringAsFixed(2)}',
                            style: TextStyle(
                              color:Colors.white,
                              fontWeight: FontWeight.bold,
                                fontSize: 25
                            ),
                            )
                        ),
                      ),
                    ),
                  ),
                ),
                title:Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          width: 1,
                          // color: Theme.of(context).primaryColorLight,
                          color: Colors.white60
                      ),
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(40)
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromARGB(255, 65, 64, 64),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(
                              0, 2), // changes position of shadow
                        )
                      ]
                  ),
                    child: Text(
                        transactions[index].title as String,
                    textAlign:TextAlign.left,
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87

                    ),),
                ),
                subtitle: Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                            width: 1,
                            // color: Theme.of(context).primaryColorLight,
                            color: Colors.white60
                        ),
                        borderRadius: BorderRadius.only(
                            bottomRight:Radius.circular(40)
                        ),
                        color: card_bg_color,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromARGB(255, 65, 64, 64),
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(
                                0, 1), // changes position of shadow
                          )
                        ]
                    ),
                    child: Text(
                      DateFormat.yMMMd().format(transactions[index].date!),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white

                      ),)),
                onLongPress: ()=>null,
                trailing: Container(
                    child: Text(index.toString())
                ),

              ),
            );
          },itemCount:transactions.length,)
    );
  }
}
