import 'package:flutter/material.dart';


class EmptyData extends StatelessWidget {
  const EmptyData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            Text("no data!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 35,
              ),
            ),
            Container(
              height: 250,
              child: Image.asset(
                'assets/image/finding.gif',
                fit:BoxFit.cover,),
            )

          ],
        ),
      ),
    );
  }
}
