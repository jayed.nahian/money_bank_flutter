import 'package:flutter/material.dart';

//for date
import 'package:intl/intl.dart';

// custom widgets
import '../model_class/transection.dart';
import '/widgets/empty_data.dart';

class TransctionList extends StatelessWidget {
  final card_bg_color = const Color(0xFFA7AAB2);
  final card_title_color = const Color(0xFF7A818A);
  final full_bg_color = const Color(0xFFA7AAB2);

  //set a variable for taking the data
  final List<Transaction> transection;
  TransctionList(this.transection);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 320,
      child:
      //data empty logic
      transection.isEmpty ?
         EmptyData() : ListView(
        children: transection.map((element) {
          return Card(
            elevation: 5,
            child: Container(
              margin: EdgeInsets.all(3),
              decoration: BoxDecoration(
                color: card_bg_color,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 65, 64, 64),
                    spreadRadius: 2,
                    blurRadius: 2,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: Row(
                children: [
                  //for id
                  Container(
                    margin: EdgeInsets.all(2),
                    //image part
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                        color: card_title_color,
                        borderRadius: BorderRadius.circular(20)),
                    // ,
                    child: Center(
                      child: Text(
                        element.id as String,
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),


                  //Price and title
                  Container(
                    child: Column(
                      // mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        //Price
                        Container(
                          margin: EdgeInsets.all(3),
                          height: 50,
                          width: 130,
                          decoration: BoxDecoration(
                              color: card_title_color,
                              borderRadius: BorderRadius.circular(15)),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 3,
                                // color: Theme.of(context).primaryColorLight,
                                  color: Colors.white70
                              ),
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                '\$ ${element.amount!.toStringAsFixed(2)}',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        // Title
                        Container(
                            width: 150,
                            decoration: BoxDecoration(
                                color: card_title_color,
                                borderRadius: BorderRadius.circular(10)),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 3,
                                  // color: Theme.of(context).primaryColorLight,
                                  color: Colors.white70
                                ),
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                              ),
                              child: Center(
                                child: Text(
                                  element.title as String,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  // for date
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        width: 5,
                        color: Theme.of(context).primaryColorLight,
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // date title
                        Container(
                            height: 30,
                            width: 120,
                            color: Colors.black,
                            child: Center(
                              child: Text(
                                'Time/Date',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 18, color: Colors.white),
                              ),
                            )),
                        //date value
                        Container(
                            height: 50,
                            width: 125,
                            // color:card_title_color,
                            decoration: BoxDecoration(
                                color: card_title_color,
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: [
                                  BoxShadow(
                                    color: Color.fromARGB(255, 65, 64, 64),
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    offset: Offset(
                                        0, 3), // changes position of shadow
                                  ),
                                ],
                                border: Border.all(
                                  width: 2,
                                  color: Theme.of(context).primaryColorLight,
                                )),
                            child: Center(
                              child: Text(
                                // DateFormat('MM').format(element.date!),
                                DateFormat.yMMMd().add_jms().format(element.date!),
                                // DateFormat().format(element.date!),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.white,
                                ),
                              ),
                            )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}
